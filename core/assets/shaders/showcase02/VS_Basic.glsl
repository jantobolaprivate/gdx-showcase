attribute vec3 a_position;
attribute vec4 a_color;

varying vec4 out_color;

uniform mat4 mvp;

void main()
{
    gl_Position = mvp * vec4(a_position, 1.0);
    out_color = a_color;
}