uniform sampler2D u_texture;

varying vec2 out_tex;

void main()
{
    gl_FragColor = texture2D(u_texture, out_tex);
}