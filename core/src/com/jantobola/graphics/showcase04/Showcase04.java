package com.jantobola.graphics.showcase04;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Showcase04
 *
 * This showcase demonstrates text drawing and it shows fps.
 *
 * @author Jan Tobola, 2015
 */
public class Showcase04 extends ApplicationAdapter {

    // TEXT (FPS draw)
    Stage stage;
    Table table;
    Label fpsLabel;
    BitmapFont font;
    FPSCounter fps;

    public boolean clear = true;

    // FPS Loger

    public static class FPSCounter {
        long startTime;
        int oldFPS;

        public FPSCounter () {
            startTime = TimeUtils.nanoTime();
        }

        /** Logs the current frames per second to the console. */
        public int fps() {
            if (TimeUtils.nanoTime() - startTime > 1000000000) /* 1,000,000,000ns == one second */{
                oldFPS = Gdx.graphics.getFramesPerSecond();
                startTime = TimeUtils.nanoTime();
            }
            return oldFPS;
        }
    }

    @Override
    public void create() {

        //super.create();

        // FONT

        fps = new FPSCounter();
        stage = new Stage();
        table = new Table();

        font = new BitmapFont();
        fpsLabel = new Label("", new Label.LabelStyle(font, Color.WHITE));

        table.setFillParent(true);
        stage.addActor(table);
        table.add(fpsLabel);
        table.left().top().padLeft(10).padTop(5);
        table.pack();

        table.setDebug(false);

    }

    @Override
    public void render() {

        //super.render();

        Gdx.gl.glCullFace(GL20.GL_FRONT);
        if(clear) Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        // draw fps on the screen
        fpsLabel.setText("FPS: " + fps.fps());

        stage.act();
        stage.draw();
        // @formatter:on
    }

    @Override
    public void dispose() {
        //super.dispose();

        stage.dispose();
        font.dispose();
    }

    @Override
    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

}
