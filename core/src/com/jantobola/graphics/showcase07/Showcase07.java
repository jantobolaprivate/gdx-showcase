package com.jantobola.graphics.showcase07;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.jantobola.graphics.showcase04.Showcase04;

/**
 * Showcase07 - LIGHTS
 *
 * @author Jan Tobola, 2015
 */
public class Showcase07 extends Showcase04 {

    private PerspectiveCamera cam;
    private Mesh cube;
    private Model model;
    private Array<ModelInstance> modelInstances = new Array<ModelInstance>();
    private ModelBatch batch;

    private FirstPersonCameraController cntrl;

    Environment env;
    Vector3 lightPos = new Vector3(1, 1.6f, 2);
    Vector3 light2Pos = new Vector3(-1, 1.6f, -2);
    PointLight pointLight = new PointLight();
    PointLight pointLight2 = new PointLight();
    Mesh light;
    Mesh light2;

    @Override
    public void create () {

        super.create();
        super.clear = false;

        float[] vertices = new float[]{
                // BACK FACE
                /* POS */ -0.5f, -0.5f, -0.5f, /* TEX */ 0, 1, /* NOR */ 0, 0, -1,
                /* POS */ -0.5f, +0.5f, -0.5f, /* TEX */ 0, 0, /* NOR */ 0, 0, -1,
                /* POS */ +0.5f, +0.5f, -0.5f, /* TEX */ 1, 0, /* NOR */ 0, 0, -1,
                /* POS */ +0.5f, -0.5f, -0.5f, /* TEX */ 1, 1, /* NOR */ 0, 0, -1,

                // FRONT FACE
                /* POS */ -0.5f, -0.5f, +0.5f, /* TEX */ 1, 1, /* NOR */ 0, 0, +1,
                /* POS */ +0.5f, -0.5f, +0.5f, /* TEX */ 0, 1, /* NOR */ 0, 0, +1,
                /* POS */ +0.5f, +0.5f, +0.5f, /* TEX */ 0, 0, /* NOR */ 0, 0, +1,
                /* POS */ -0.5f, +0.5f, +0.5f, /* TEX */ 1, 0, /* NOR */ 0, 0, +1,

                // TOP FACE
                /* POS */ -0.5f, +0.5f, -0.5f, /* TEX */ 0, 1, /* NOR */ 0, +1, 0,
                /* POS */ -0.5f, +0.5f, +0.5f, /* TEX */ 0, 0, /* NOR */ 0, +1, 0,
                /* POS */ +0.5f, +0.5f, +0.5f, /* TEX */ 1, 0, /* NOR */ 0, +1, 0,
                /* POS */ +0.5f, +0.5f, -0.5f, /* TEX */ 1, 1, /* NOR */ 0, +1, 0,

                // BOTTOM FACE
                /* POS */ -0.5f, -0.5f, -0.5f, /* TEX */ 1, 1, /* NOR */ 0, -1, 0,
                /* POS */ +0.5f, -0.5f, -0.5f, /* TEX */ 0, 1, /* NOR */ 0, -1, 0,
                /* POS */ +0.5f, -0.5f, +0.5f, /* TEX */ 0, 0, /* NOR */ 0, -1, 0,
                /* POS */ -0.5f, -0.5f, +0.5f, /* TEX */ 1, 0, /* NOR */ 0, -1, 0,

                // LEFT FACE
                /* POS */ -0.5f, -0.5f, +0.5f, /* TEX */ 0, 1, /* NOR */ -1, 0, 0,
                /* POS */ -0.5f, +0.5f, +0.5f, /* TEX */ 0, 0, /* NOR */ -1, 0, 0,
                /* POS */ -0.5f, +0.5f, -0.5f, /* TEX */ 1, 0, /* NOR */ -1, 0, 0,
                /* POS */ -0.5f, -0.5f, -0.5f, /* TEX */ 1, 1, /* NOR */ -1, 0, 0,

                // RIGHT FACE
                /* POS */  +0.5f, -0.5f, -0.5f, /* TEX */ 0, 1, /* NOR */ +1, 0, 0,
                /* POS */  +0.5f, +0.5f, -0.5f, /* TEX */ 0, 0, /* NOR */ +1, 0, 0,
                /* POS */  +0.5f, +0.5f, +0.5f, /* TEX */ 1, 0, /* NOR */ +1, 0, 0,
                /* POS */  +0.5f, -0.5f, +0.5f, /* TEX */ 1, 1, /* NOR */ +1, 0, 0
        };

        short[] indices = new short[]{
                // BACK FACE
                0,  1, 2,
                0,  2, 3,

                // FRONT FACE
                4,  5, 6,
                4,  6, 7,

                // TOP FACE
                8,  9, 10,
                8,  10, 11,

                // BOTTOM FACE
                12, 13, 14,
                12, 14, 15,

                // LEFT FACE
                16, 17, 18,
                16, 18, 19,

                // RIGHT FACE
                20, 21, 22,
                20, 22, 23
        };

        cube = new Mesh(true, vertices.length, indices.length,
                new VertexAttributes(
                        new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
                        new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE),
                        new VertexAttribute(VertexAttributes.Usage.Normal, 3, ShaderProgram.NORMAL_ATTRIBUTE)
                )
        );
        cube.setVertices(vertices);
        cube.setIndices(indices);

        Material material = new Material(
                IntAttribute.createCullFace(Gdx.gl.GL_BACK)
        );

        Material debug = new Material(
                IntAttribute.createCullFace(Gdx.gl.GL_BACK),
                ColorAttribute.createAmbient(Color.WHITE),
                ColorAttribute.createDiffuse(Color.WHITE)
        );

        light = cube.copy(true);
        light2 = cube.copy(true);

        ModelBuilder builder = new ModelBuilder();

        builder.begin();
        builder.part("cube", cube, Gdx.gl.GL_TRIANGLES, material);
        builder.part("light", light, Gdx.gl.GL_TRIANGLES, debug).mesh.transform(new Matrix4().trn(lightPos).scale(0.2f, 0.2f, 0.2f));
        builder.part("light2", light2, Gdx.gl.GL_TRIANGLES, debug).mesh.transform(new Matrix4().trn(light2Pos).scale(0.2f, 0.2f, 0.2f));
        model = builder.end();

        modelInstances.add(new ModelInstance(model));
        batch = new ModelBatch();

// ------------ camera -------------

        cam = new PerspectiveCamera(62, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.z = 5;
        cam.lookAt(0, 0, 0);

        cam.near = 0.1f;
        cam.far = 100;

        cam.update();

        cntrl = new FirstPersonCameraController(cam);
        Gdx.input.setInputProcessor(cntrl);

        // ENVIRONMENT

        env = new Environment();
        env.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.1f, 0.1f, 0.1f, 1f));
        env.add(pointLight.set(Color.GREEN, lightPos, 6));
        env.add(pointLight2.set(Color.BLUE, light2Pos, 6));

    }

    @Override
    public void render () {

        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        cntrl.update();

        batch.begin(cam);
            batch.render(modelInstances, env);
        batch.end();

        super.render();
    }

    @Override
    public void dispose() {
        model.dispose();
        batch.dispose();
        super.dispose();
    }

}
