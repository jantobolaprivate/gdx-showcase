package com.jantobola.graphics.showcase05;

import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.collision.btBoxShape;
import com.badlogic.gdx.physics.bullet.collision.btCollisionShape;
import com.badlogic.gdx.physics.bullet.dynamics.btRigidBody;
import com.badlogic.gdx.utils.Disposable;

/**
 * SimplestObject3D
 *
 * @author Jan Tobola, 2015
 */
public class SimplestObject3D implements Disposable {

        public Matrix4 world = new Matrix4();
        public int topology = GL20.GL_TRIANGLES;
        public Mesh mesh;

        public btRigidBody body;
        public SimpleMotionState motionState;

        public btRigidBody.btRigidBodyConstructionInfo constructionInfo;
        public btCollisionShape shape;

        public SimplestObject3D() {
            motionState = new SimpleMotionState();
            motionState.transform = world;
        }

        public static SimplestObject3D createBox(float mass) {

            SimplestObject3D object = new SimplestObject3D();

            object.shape = new btBoxShape(new Vector3(0.5f, 0.5f, 0.5f));
            Vector3 localInertia = new Vector3(0, 0, 0);

            if (mass > 0) {
                object.shape.calculateLocalInertia(mass, localInertia);
            } else {
                localInertia.set(0, 0, 0);
            }

            object.constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(mass, null, object.shape, localInertia);
            object.body = new btRigidBody(object.constructionInfo);
            object.body.setMotionState(object.motionState);
            object.body.setWorldTransform(new Matrix4().trn(0.5f, 0.5f, 0.5f));

            // @formatter:off

//			  CUBE GEOMETRY
//			"""""""""""""""""
//				y
//				|[4]         [5]
//				o * * * * * o
//			   *|          **
//			  * |     [7] * *
//		 [6] o *|*_*_*_*_o__*__ x
//			 * /o [0]    *  o [1]
//			 */          * *
//			 *	         **
//		    /o * * * * * o
//		   /[2]			  [3]
//		  z


            // center of mass in [0, 0, 0]
            // dont know how to shift CoM in bullet
            float[] vertices = new float[] {
                // FRONT FACE
                /* POS */ -0.5f, -0.5f, -0.5f, /* TEX */ 0, 1,
                /* POS */ -0.5f, +0.5f, -0.5f, /* TEX */ 0, 0,
                /* POS */ +0.5f, +0.5f, -0.5f, /* TEX */ 1, 0,
                /* POS */ +0.5f, -0.5f, -0.5f, /* TEX */ 1, 1,

                // BACK FACE
                /* POS */ -0.5f, -0.5f, +0.5f, /* TEX */ 1, 1,
                /* POS */ +0.5f, -0.5f, +0.5f, /* TEX */ 0, 1,
                /* POS */ +0.5f, +0.5f, +0.5f, /* TEX */ 0, 0,
                /* POS */ -0.5f, +0.5f, +0.5f, /* TEX */ 1, 0,

                // TOP FACE
                /* POS */ -0.5f, +0.5f, -0.5f, /* TEX */ 0, 1,
                /* POS */ -0.5f, +0.5f, +0.5f, /* TEX */ 0, 0,
                /* POS */ +0.5f, +0.5f, +0.5f, /* TEX */ 1, 0,
                /* POS */ +0.5f, +0.5f, -0.5f, /* TEX */ 1, 1,

                // BOTTOM FACE
                /* POS */ -0.5f, -0.5f, -0.5f, /* TEX */ 1, 1,
                /* POS */ +0.5f, -0.5f, -0.5f, /* TEX */ 0, 1,
                /* POS */ +0.5f, -0.5f, +0.5f, /* TEX */ 0, 0,
                /* POS */ -0.5f, -0.5f, +0.5f, /* TEX */ 1, 0,

                // LEFT FACE
                /* POS */ -0.5f, -0.5f, +0.5f, /* TEX */ 0, 1,
                /* POS */ -0.5f, +0.5f, +0.5f, /* TEX */ 0, 0,
                /* POS */ -0.5f, +0.5f, -0.5f, /* TEX */ 1, 0,
                /* POS */ -0.5f, -0.5f, -0.5f, /* TEX */ 1, 1,

                // RIGHT FACE
                /* POS */  +0.5f, -0.5f, -0.5f, /* TEX */ 0, 1,
                /* POS */  +0.5f, +0.5f, -0.5f, /* TEX */ 0, 0,
                /* POS */  +0.5f, +0.5f, +0.5f, /* TEX */ 1, 0,
                /* POS */  +0.5f, -0.5f, +0.5f, /* TEX */ 1, 1,
            };

            short[] indices = new short[] {
                // FRONT FACE
                0,  1, 2,
                0,  2, 3,

                // BACK FACE
                4,  5, 6,
                4,  6, 7,

                // TOP FACE
                8,  9, 10,
                8,  10, 11,

                // BOTTOM FACE
                12, 13, 14,
                12, 14, 15,

                // LEFT FACE
                16, 17, 18,
                16, 18, 19,

                // RIGHT FACE
                20, 21, 22,
                20, 22, 23,
            };

            object.mesh = new Mesh(true, vertices.length, indices.length,
                    new VertexAttributes(
                            new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
                            new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE)
                    )
            );

            object.mesh.setVertices(vertices);
            object.mesh.setIndices(indices);

            // @formatter:on

            return object;
        }

        public void render(ShaderProgram shader) {
            mesh.render(shader, topology);
        }

        @Override
        public void dispose() {
            mesh.dispose();
            body.dispose();
            motionState.dispose();
            constructionInfo.dispose();
            shape.dispose();
        }

}
