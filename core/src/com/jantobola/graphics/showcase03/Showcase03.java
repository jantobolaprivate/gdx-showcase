package com.jantobola.graphics.showcase03;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.*;
import com.badlogic.gdx.physics.bullet.dynamics.*;
import com.badlogic.gdx.physics.bullet.linearmath.btMotionState;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * Using Bullet physics
 *
 * This showcase will be more complex. We need to generate a geometry for a floor using some triangle strip
 * 2D grid. We also need to learn how Bullet Physics works a modify each objects to work correctly with framework.
 *
 * This showcase should represent a 3D scene with gravity. There will be two boxes (cubes = simple geometry).
 *
 * @author Jan Tobola, 2015
 */
public class Showcase03 extends ApplicationAdapter {

    private ShaderProgram shader;

    private PerspectiveCamera cam;
    private FirstPersonCameraController cntrl;

    private SimplestObject box1;
    private SimplestObject box2;

    // BULLET PHYSICS OBJECTS

    btCollisionConfiguration collisionConfig;
    btDispatcher dispatcher;
    SimpleContactListener contactListener;
    btBroadphaseInterface broadphase;
    btDynamicsWorld dynamicsWorld;
    btConstraintSolver constraintSolver;

    public static Vector3 zeroVec = new Vector3(0, 0, 0);

    // OBJECT CLASS

    public static class SimplestObject implements Disposable {

        public Matrix4 world = new Matrix4();
        public int topology = GL20.GL_TRIANGLES;
        public Mesh mesh;

        public btRigidBody body;
        public SimpleMotionState motionState;

        public btRigidBody.btRigidBodyConstructionInfo constructionInfo;
        public btCollisionShape shape;

        public SimplestObject() {
            motionState = new SimpleMotionState();
            motionState.transform = world;
        }

        public static SimplestObject createBox(float mass) {

            SimplestObject object = new SimplestObject();

            object.shape = new btBoxShape(new Vector3(0.5f, 0.5f, 0.5f));
            Vector3 localInertia = new Vector3(0, 0, 0);

            if (mass > 0) {
                object.shape.calculateLocalInertia(mass, localInertia);
            } else {
                localInertia.set(0, 0, 0);
            }

            object.constructionInfo = new btRigidBody.btRigidBodyConstructionInfo(mass, null, object.shape, localInertia);
            object.body = new btRigidBody(object.constructionInfo);
            object.body.setMotionState(object.motionState);
            object.body.setWorldTransform(new Matrix4().trn(0.5f, 0.5f, 0.5f));

            // @formatter:off

//			  CUBE GEOMETRY
//			"""""""""""""""""
//				y
//				|[4]         [5]
//				o * * * * * o
//			   *|          **
//			  * |     [7] * *
//		 [6] o *|*_*_*_*_o__*__ x
//			 * /o [0]    *  o [1]
//			 */          * *
//			 *	         **
//		    /o * * * * * o
//		   /[2]			  [3]
//		  z


            // center of mass in [0, 0, 0]
            // dont know how to shift CoM in bullet
            float[] vertices = new float[] {
                    -0.5f, -0.5f, -0.5f, Color.RED.toFloatBits(), 		// [0]
                    +0.5f, -0.5f, -0.5f, Color.GREEN.toFloatBits(),		// [1]
                    -0.5f, -0.5f, +0.5f, Color.BLUE.toFloatBits(),		// [2]
                    +0.5f, -0.5f, +0.5f, Color.WHITE.toFloatBits(),		// [3]

                    -0.5f, +0.5f, -0.5f, Color.RED.toFloatBits(),		// [4]
                    +0.5f, +0.5f, -0.5f, Color.GREEN.toFloatBits(),		// [5]
                    -0.5f, +0.5f, +0.5f, Color.BLUE.toFloatBits(),		// [6]
                    +0.5f, +0.5f, +0.5f, Color.WHITE.toFloatBits()		// [7]
            };

            short[] indices = new short[] {
                    // [front]
                    2, 3, 6,
                    6, 3, 7,
                    // [top]
                    6, 7, 4,
                    4, 7, 5,
                    // [back]
                    1, 0, 5,
                    5, 0, 4,
                    // [bottom]
                    2, 0, 3,
                    3, 0, 1,
                    // [left]
                    0, 2, 4,
                    4, 2, 6,
                    // [right]
                    3, 1, 7,
                    7, 1, 5
            };

            object.mesh = new Mesh(true, vertices.length, indices.length,
                    new VertexAttributes(
                            new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
                            new VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE)
                    )
            );

            object.mesh.setVertices(vertices);
            object.mesh.setIndices(indices);

            // @formatter:on

            return object;
        }

        public void render(ShaderProgram shader) {
            mesh.render(shader, topology);
        }

        @Override
        public void dispose() {
            mesh.dispose();
            body.dispose();
            motionState.dispose();
            constructionInfo.dispose();
            shape.dispose();
        }
    }

    class SimpleContactListener extends ContactListener {
        @Override
        public boolean onContactAdded(int userValue0, int partId0, int index0, int userValue1, int partId1, int index1) {
            return true;
        }
    }

    static class SimpleMotionState extends btMotionState {
        Matrix4 transform;
        @Override
        public void getWorldTransform (Matrix4 worldTrans) {
            worldTrans.set(transform);
        }
        @Override
        public void setWorldTransform (Matrix4 worldTrans) {
            transform.set(worldTrans);
        }
    }

    @Override
    public void create() {

        Bullet.init();

        box1 = SimplestObject.createBox(0);
        box2 = SimplestObject.createBox(1);

        box1.body.setCollisionFlags(box1.body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_KINEMATIC_OBJECT);
        box1.world.trn(-0.5f, 0, 0.5f);
        box1.body.proceedToTransform(box1.world);

        box2.body.setCollisionFlags(box2.body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
        box2.world.trn(new Vector3(0, 7, 0));
        box2.body.proceedToTransform(box2.world);

        // ------------ camera -------------

        cam = new PerspectiveCamera(65, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(2.6959727f, 4.8875647f, 1.9147941f);
        cam.direction.set(-0.53266466f, -0.7489429f, -0.39414626f);

        cam.near = 0.1f;
        cam.far = 100;

        cntrl = new FirstPersonCameraController(cam);
        Gdx.input.setInputProcessor(cntrl);

        // ------------ shaders -------------

        shader = new ShaderProgram(Gdx.files.internal("shaders/showcase03/VS_Basic.glsl"), Gdx.files.internal("shaders/showcase03/PS_Basic.glsl"));
        if (!shader.isCompiled())
            throw new GdxRuntimeException("Couldn't compile shader: " + shader.getLog());

        // ------------ inition rendering context -----------

        Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);

        // PHYSICS

        collisionConfig = new btDefaultCollisionConfiguration();
        dispatcher = new btCollisionDispatcher(collisionConfig);
        broadphase = new btDbvtBroadphase();
        constraintSolver = new btSequentialImpulseConstraintSolver();
        dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, constraintSolver, collisionConfig);
        dynamicsWorld.setGravity(new Vector3(0, -10f, 0));
        contactListener = new SimpleContactListener();

        // add objects to physics world
        dynamicsWorld.addRigidBody(box1.body);
        dynamicsWorld.addRigidBody(box2.body);

    }

    @Override
    public void render() {

        final float delta = Math.min(1f / 30f, Gdx.graphics.getDeltaTime());
        dynamicsWorld.stepSimulation(delta, 5, 1.0f/60.0f);
        cntrl.update();

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        // @formatter:off

        shader.begin();
            shader.setUniformMatrix("worldMatrix", box1.world);
            shader.setUniformMatrix("viewMatrix", cam.view);
            shader.setUniformMatrix("projectionMatrix", cam.projection);
            box1.render(shader);

            shader.setUniformMatrix("worldMatrix", box2.world);
            box2.render(shader);
        shader.end();

        if(box2.world.val[Matrix4.M13] < -20) {
            box2.world.idt();
            box2.world.trn(0, 7, 0);
            box2.body.proceedToTransform(box2.world);

            box2.body.clearForces();
            box2.body.setLinearVelocity(zeroVec);
            box2.body.setAngularVelocity(zeroVec);

            box2.body.activate();
        }

        // @formatter:on
    }

    @Override
    public void dispose() {
        box1.dispose();
        box2.dispose();
        shader.dispose();

        dynamicsWorld.dispose();
        constraintSolver.dispose();
        broadphase.dispose();
        dispatcher.dispose();
        collisionConfig.dispose();
        contactListener.dispose();
    }

}
