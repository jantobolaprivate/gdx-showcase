package com.jantobola.graphics.showcase01;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Array;

/**
 * Custom shaders
 *
 * Remarks:
 * 1) Wireframe is possible to use only for desktop mode
 * 2) It will be better to use own shaders instead of DefaultShader, better to not using DefaultShader at all, because
 * 		when version of opengl is changed to higher value, errors occurs because some shader version numbers are missing
 * 		inside a shader code.
 *
 */
public class Showcase01 extends ApplicationAdapter {
	private PerspectiveCamera cam;
	private Mesh cube;
	private Model worldFragment;
	private Array<ModelInstance> modelInstances = new Array<ModelInstance>();
	private ModelBatch batch;

	private FirstPersonCameraController cntrl;

	@Override
	public void create () {
		cube = new Mesh(true, 8, 36,
				new VertexAttributes(
						new VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
						new VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE)
				)
		);

		Material material = new Material(
				IntAttribute.createCullFace(Gdx.gl.GL_NONE),
				ColorAttribute.createAmbient(Color.WHITE)
		);

		cube.setVertices(new float[]{
				-0.5f, -0.5f, -0.5f, Color.RED.toFloatBits(), 		// [0]
				+0.5f, -0.5f, -0.5f, Color.GREEN.toFloatBits(),		// [1]
				-0.5f, -0.5f, +0.5f, Color.BLUE.toFloatBits(),		// [2]
				+0.5f, -0.5f, +0.5f, Color.WHITE.toFloatBits(),		// [3]

				-0.5f, +0.5f, -0.5f, Color.RED.toFloatBits(),		// [4]
				+0.5f, +0.5f, -0.5f, Color.GREEN.toFloatBits(),		// [5]
				-0.5f, +0.5f, +0.5f, Color.BLUE.toFloatBits(),		// [6]
				+0.5f, +0.5f, +0.5f, Color.WHITE.toFloatBits()		// [7]
		});


//				y
//				|[4]         [5]
//				o * * * * * o
//			   *|          **
//			  * |     [7] * *
//		 [6] o *|*_*_*_*_o__*__ x
//			 * /o [0]    *  o [1]
//			 */          * *
//			 *	         **
//		    /o * * * * * o
//		   /[2]			  [3]
//		  z

		cube.setIndices(new short[]{
				// [front]
				2, 3, 6,
				6, 3, 7,
				// [top]
				6, 7, 4,
				4, 7, 5,
				// [back]
				1, 0, 5,
				5, 0, 4,
				// [bottom]
				2, 0, 3,
				3, 0, 1,
				// [left]
				0, 2, 4,
				4, 2, 6,
				// [right]
				3, 1, 7,
				7, 1, 5
		});

		ModelBuilder builder = new ModelBuilder();

		builder.begin();
		builder.part("cube", cube, Gdx.gl.GL_TRIANGLES, material);
		worldFragment = builder.end();

		modelInstances.add(new ModelInstance(worldFragment));
		batch = new ModelBatch();

// ------------ camera -------------

		cam = new PerspectiveCamera(75, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam.position.z = 5;
		cam.lookAt(0, 0, 0);

		cam.near = 0.1f;
		cam.far = 100;

		cam.update();

		cntrl = new FirstPersonCameraController(cam);
		Gdx.input.setInputProcessor(cntrl);

	}

	@Override
	public void render () {

		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		cam.update();
		cntrl.update();

		batch.begin(cam);
			batch.render(modelInstances);
		batch.end();
	}

	@Override
	public void dispose() {
		worldFragment.dispose();
		batch.dispose();
	}
}
