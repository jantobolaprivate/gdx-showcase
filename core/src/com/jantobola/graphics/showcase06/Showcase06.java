package com.jantobola.graphics.showcase06;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.*;
import com.badlogic.gdx.physics.bullet.dynamics.btConstraintSolver;
import com.badlogic.gdx.physics.bullet.dynamics.btDiscreteDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btDynamicsWorld;
import com.badlogic.gdx.physics.bullet.dynamics.btSequentialImpulseConstraintSolver;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.jantobola.graphics.showcase04.Showcase04;
import com.jantobola.graphics.showcase05.SimpleContactListener;
import com.jantobola.graphics.showcase05.SimplestObject3D;

import java.nio.FloatBuffer;

/**
 * Showcase06 RENDER TO TEXTURE
 *
 * @author Jan Tobola, 2015
 */
public class Showcase06 extends Showcase04 {

    private ShaderProgram shader;

    private PerspectiveCamera cam;
    private FirstPersonCameraController cntrl;

    private SimplestObject3D box1;
    private SimplestObject3D box2;

    // BULLET PHYSICS OBJECTS

    btCollisionConfiguration collisionConfig;
    btDispatcher dispatcher;
    SimpleContactListener contactListener;
    btBroadphaseInterface broadphase;
    btDynamicsWorld dynamicsWorld;
    btConstraintSolver constraintSolver;

    public static Vector3 zeroVec = new Vector3(0, 0, 0);

    // TEXTURE

    Texture tex;
    FrameBuffer renderTex;

    @Override
    public void create() {
        Bullet.init();

        box1 = SimplestObject3D.createBox(0);
        box2 = SimplestObject3D.createBox(1);

        box1.body.setCollisionFlags(box1.body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_KINEMATIC_OBJECT);
        box1.world.trn(-0.5f, 0, 0.5f);
        box1.body.proceedToTransform(box1.world);

        box2.body.setCollisionFlags(box2.body.getCollisionFlags() | btCollisionObject.CollisionFlags.CF_CUSTOM_MATERIAL_CALLBACK);
        box2.world.trn(new Vector3(0, 7, 0));
        box2.body.proceedToTransform(box2.world);

        // ------------ camera -------------

        cam = new PerspectiveCamera(65, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(2.6959727f, 4.8875647f, 1.9147941f);
        cam.direction.set(-0.53266466f, -0.7489429f, -0.39414626f);

        cam.near = 0.1f;
        cam.far = 100;

        cntrl = new FirstPersonCameraController(cam);
        cntrl.setDegreesPerPixel(0.2f);
        Gdx.input.setInputProcessor(cntrl);

        // ------------ shaders -------------

        shader = new ShaderProgram(Gdx.files.internal("shaders/showcase05/VS_Basic.glsl"), Gdx.files.internal("shaders/showcase05/PS_Basic.glsl"));
        if (!shader.isCompiled())
            throw new GdxRuntimeException("Couldn't compile shader: " + shader.getLog());

        // ------------ inition rendering context -----------

        Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
        Gdx.gl.glEnable(GL20.GL_CULL_FACE);

        // PHYSICS

        collisionConfig = new btDefaultCollisionConfiguration();
        dispatcher = new btCollisionDispatcher(collisionConfig);
        broadphase = new btDbvtBroadphase();
        constraintSolver = new btSequentialImpulseConstraintSolver();
        dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, constraintSolver, collisionConfig);
        dynamicsWorld.setGravity(new Vector3(0, -10f, 0));
        contactListener = new SimpleContactListener();

        // add objects to physics world
        dynamicsWorld.addRigidBody(box1.body);
        dynamicsWorld.addRigidBody(box2.body);

        super.create();
        super.clear = false;

        // TEXTURE
        tex = new Texture(Gdx.files.internal("textures/crate.jpg"), true);
        tex.setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.MipMapLinearLinear);
        tex.bind();

        // This is a method how the anisotropic filtering can be enabled, but first we have to make sure
        // the device running this code supports such an extension for this type of filtering.
        String extensions = Gdx.gl.glGetString(GL20.GL_EXTENSIONS);
        if(extensions.contains("GL_EXT_texture_filter_anisotropic")) {
            // If it supports filtering we have to know the maximum supported value
            // of filtering. We can set the maximum value or something lesser then that.
            FloatBuffer maxAnisotropy = BufferUtils.newFloatBuffer(16);
            Gdx.gl.glGetFloatv(GL20.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
            Gdx.gl.glTexParameterf(tex.glTarget, GL20.GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy.get(0));
        }

        renderTex = new FrameBuffer(Pixmap.Format.RGBA8888, 512, 512, true);
    }

    @Override
    public void render() {
        final float delta = Math.min(1f / 30f, Gdx.graphics.getDeltaTime());
        dynamicsWorld.stepSimulation(delta, 5, 1.0f / 60.0f);
        cntrl.update();

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        // @formatter:off

        Gdx.gl.glCullFace(GL20.GL_BACK);
        shader.begin();

            renderTex.begin();
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
                tex.bind(0);
                shader.setUniformi("u_texture", 0);

                shader.setUniformMatrix("worldMatrix", box1.world);
                shader.setUniformMatrix("viewMatrix", cam.view);
                shader.setUniformMatrix("projectionMatrix", cam.projection);
                box1.render(shader);

                shader.setUniformMatrix("worldMatrix", box2.world);
                box2.render(shader);
            renderTex.end();

            renderTex.getColorBufferTexture().bind(0);
            shader.setUniformi("u_texture", 0);

            shader.setUniformMatrix("worldMatrix", box1.world);
            shader.setUniformMatrix("viewMatrix", cam.view);
            shader.setUniformMatrix("projectionMatrix", cam.projection);
            box1.render(shader);

            shader.setUniformMatrix("worldMatrix", box2.world);
            box2.render(shader);
        shader.end();

        if(box2.world.val[Matrix4.M13] < -20) {
            box2.world.idt();
            box2.world.trn(0, 7, 0);
            box2.body.proceedToTransform(box2.world);

            box2.body.clearForces();
            box2.body.setLinearVelocity(zeroVec);
            box2.body.setAngularVelocity(zeroVec);

            box2.body.activate();
        }

        super.render();
        // @formatter:on
    }

    @Override
    public void dispose() {
        box1.dispose();
        box2.dispose();
        shader.dispose();

        dynamicsWorld.dispose();
        constraintSolver.dispose();
        broadphase.dispose();
        dispatcher.dispose();
        collisionConfig.dispose();
        contactListener.dispose();

        super.dispose();

        tex.dispose();
        contactListener.dispose();
        renderTex.dispose();
    }

}
