package com.jantobola.graphics.desktop;

import com.badlogic.gdx.*;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.lwjgl.opengl.GL11;

import javax.swing.*;

public class DesktopLauncher {
	public static void main (String[] arg) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.useGL30 = false;
		config.width = 1024;
		config.height = 768;
		config.foregroundFPS = 0;
		config.samples = 8;
		config.vSyncEnabled = true;

		Class c = null;

		if(arg.length > 0 && arg[0] != null) {
			c = Class.forName(arg[0]);
		} else {
			String res = JOptionPane.showInputDialog(null, "Enter showcase number (e.g. 01):", "Showcase #", JOptionPane.INFORMATION_MESSAGE);
			String showcaseClass = "com.jantobola.graphics.showcase" + res + ".Showcase" + res;
			c = Class.forName(showcaseClass);
		}

		final ApplicationAdapter adapter = (ApplicationAdapter) c.newInstance();

		ApplicationListener desktopWrapper = new ApplicationListener() {
			@Override
			public void create() {
				adapter.create();

				InputMultiplexer multiplexer = new InputMultiplexer();
				multiplexer.addProcessor(new InputAdapter() {
					@Override
					public boolean keyDown(int keycode) {
						if(keycode == Input.Keys.F2) {
							GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
							return false;
						}

						if(keycode == Input.Keys.F1) {
							GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_FILL);
							return false;
						}

						return false;
					}
				});
				if(Gdx.input.getInputProcessor() != null) multiplexer.addProcessor(Gdx.input.getInputProcessor());
				Gdx.input.setInputProcessor(multiplexer);
			}
			@Override
			public void resize(int width, int height) {
				adapter.resize(width, height);
			}
			@Override
			public void render() {
				adapter.render();
			}
			@Override
			public void pause() {
				adapter.pause();
			}
			@Override
			public void resume() {
				adapter.resume();
			}
			@Override
			public void dispose() {
				adapter.dispose();
			}
		};

		new LwjglApplication(desktopWrapper, config);
	}
}
